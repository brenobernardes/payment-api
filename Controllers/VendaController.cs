using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaymentAPI.Context;
using PaymentAPI.Entities;

namespace PaymentAPI.Controller
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly RegistrarVenda _context;
        public VendaController(RegistrarVenda context)
        {
            _context = context;
        }

        [HttpPost("RegistarVendedor")]
        public IActionResult RegistrarVendedor(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }

        [HttpPost("RegistarProduto")]
        public IActionResult RegistrarProduto(Produto produto)
        {
            _context.Add(produto);
            _context.SaveChanges();
            return Ok(produto);
        }

        [HttpPost("RegistarVenda")]
        public IActionResult RegistrarVenda(Venda venda)
        {
            venda.DataVenda = DateTime.Now;
            
            _context.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }
    }
}