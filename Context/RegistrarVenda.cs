using Microsoft.EntityFrameworkCore;
using PaymentAPI.Entities;

namespace PaymentAPI.Context
{
    public class RegistrarVenda : DbContext
    {
        public RegistrarVenda(DbContextOptions<RegistrarVenda> options) : base(options)
        {

        }

        public DbSet<Vendedor> Vendedor { get; set; }
        public DbSet<Produto> Produto { get; set; }
        public DbSet<Venda> Venda { get; set; }
    }
}