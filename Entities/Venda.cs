namespace PaymentAPI.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public DateTime DataVenda { get; set; }
        public Produto Produtos { get; set; }
        public Vendedor IdVendedor { get; }
    }
}